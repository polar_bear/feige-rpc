####飞鸽RPC是一个非常轻量级，且容易修改整合到实际项目中的远程调用框架，基于NIO框架netty开发，轻松支持高并发，支持服务负载均衡，无缝整合spring

##一、provider配置

服务器配置 feige.properties 放在classpath下

    #服务绑定的ip，可选
    feige.host=127.0.0.1
    #服务端口
    feige.port=10221
    #服务调用时处理线程池线程数量
    feige.poolsize=50
    feige.host=127.0.0.1
	#协议以及对象序列化配置，本配置为可选，不配置则对象序列化使用java原生序列化
	feige.protocol=io.feige.rpc.protocol.kryo.KryoObjectProtocol

###服务导出，提供两种方式，任选一种即可

####1. API导出服务

    RpcProducerService rpcProducerService = new RpcProducerService(); //该对象全局唯一即可，不能重复实例化
    rpcProducerService.start();
    rpcProducerService.exportService(serviceInterfaceName, serviceObj);
####2. spring配置

    <?xml version="1.0" encoding="UTF-8"?>
    <beans xmlns="http://www.springframework.org/schema/beans" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:feige="http://www.feige.io/feige" xsi:schemaLocation="http://www.springframework.org/schema/beans   http://www.springframework.org/schema/beans/spring-beans.xsd http://www.feige.io/feige http://www.feige.io/feige/feige.xsd">
        <bean id="testService" class="io.feige.rpc.test.provider.impl.TestServiceImpl"/> 
        <!-- 导出服务 --></br>
        <feige:service interface="io.feige.rpc.test.provider.TestService" ref="testService" /> 
    </beans>

##二、consumer使用配置

配置 feige.properties 放在classpath下

	#协议以及对象序列化配置，本配置为可选，不配置则对象序列化使用java原生序列化；如果一个应用同时包括服务提供者和调用者 feige.properties配置文件可以合并
	feige.protocol=io.feige.rpc.protocol.kryo.KryoObjectProtocol

需要调用的服务配置，每个服务可以配置多个服务提供者，即host的配置，可以根据你的配置自动进行负载均衡

    feige-import.xml 该文件放在classpath:feige/feige-import.xml
    <?xml version="1.0" encoding="UTF-8"?>	
    <remote-services>
	        <remote-service> 
		        <interface>io.feige.rpc.TestService</interface> 
				<invoke-timeout>30</invoke-timeout> 
				<hosts> 
					<host> 
						<ip>130.255.2.107</ip> 
						<port>10221</port> 
						<!--权重，数字越大权重越高--> 
						<weight>1</weight> 
					</host> 
					<host> 
						<ip>130.255.2.107</ip> 
						<port>10222</port> 
					    <weight>2</weight> 
			        </host> 
		        </hosts>
	    </remote-service>
    </remote-services>

###服务调用
####1. API调用

    RpcConsumerService rpcConsumerService=new RpcConsumerService();//该对象全局唯一即可，不要重复实例化 
    rpcConsumerService.start("feige/feige-import.xml"); 
    //由于与服务器建连需要一定时间，还没有建连成功就调用会出异常，所以这睡眠2秒，便于测试 
    Thread.sleep(2000); 
    TestService service=rpcConsumerService.getProxyService(TestService.class); 
    service.test();

####2. spring整合
 
    <?xml version="1.0" encoding="UTF-8"?> 
    <beans xmlns="http://www.springframework.org/schema/beans" 
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:feige="http://www.feige.io/feige" xsi:schemaLocation="http://www.springframework.org/schema/beans   http://www.springframework.org/schema/beans/spring-beans.xsd http://www.feige.io/feige http://www.feige.io/feige/feige.xsd">
    	<!-- 导入服务 -->
    	<feige:reference interface="io.feige.rpc.test.provider.TestService" id="testService" /> 
    </beans>

##三、后续
监控、自动发现服务、性能优化、多语言

##四、特别说明
1.feige.protocol配置为io.feige.rpc.protocol.kryo.KryoObjectProtocol时，所有类必需有默认构造方法，否则会出错，因为kryo依赖默认构造方法，但kryo序列化效率确实比java原生序列化高</br>
2.服务提供者与调用者feige.protocol如果要配置，配置必需一致
##五、下载
jar包下载：[http://www.feige.io/rpc/](http://www.feige.io/rpc/)<br />
示例项目：[http://www.feige.io/rpc/samples/](http://www.feige.io/rpc/samples/)<br />