package io.feige.rpc.consumer.network.listener;

public interface InvokeListener<T> {

	void success(T t);
	
	void failure(Throwable e);
	
}
