package io.feige.rpc.consumer.config;

import java.io.Serializable;
import java.util.List;

public class RemoteServiceConfig implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 654057930468808062L;

	private String service; 
	
	private int timeout=30;
	
	private List<ServiceSocketAddress> hosts;

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public List<ServiceSocketAddress> getHosts() {
		return hosts;
	}

	public void setHosts(List<ServiceSocketAddress> hosts) {
		this.hosts = hosts;
	}

	@Override
	public String toString() {
		return "RemoteServiceConfig [service=" + service + ", timeout="
				+ timeout + ", hosts=" + hosts + "]";
	}
 
}
