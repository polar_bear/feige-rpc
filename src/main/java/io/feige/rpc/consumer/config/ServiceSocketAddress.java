package io.feige.rpc.consumer.config;

import java.net.InetSocketAddress;

public class ServiceSocketAddress extends InetSocketAddress {
 
	private static final long serialVersionUID = 3323253052369167194L;

	private int weight;
	
	public ServiceSocketAddress(String addr, int port) {
		super(addr, port);
	}

	public ServiceSocketAddress(String addr, int port, int weight) {
		super(addr, port); 
		this.weight=weight;
	}
	
	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}
 
}
