package io.feige.rpc.exception;

public class RpcException extends RuntimeException {

	public RpcException(Throwable cause) {
		super(cause);
	}

	public RpcException() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
