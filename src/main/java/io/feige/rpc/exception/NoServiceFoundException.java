package io.feige.rpc.exception;

public class NoServiceFoundException extends RuntimeException {

	public NoServiceFoundException(String servcie) {
		super(servcie);
	}

	public NoServiceFoundException() {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 3723780753633660497L;

}
