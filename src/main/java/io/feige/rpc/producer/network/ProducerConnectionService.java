package io.feige.rpc.producer.network;

import io.feige.rpc.producer.RpcProducerService;
import io.feige.rpc.producer.config.ProducerConfig;

public interface ProducerConnectionService {

	void start(RpcProducerService rpcProducerService, ProducerConfig producerConfig);
	
	void stop();
	
}
