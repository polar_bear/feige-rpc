package io.feige.rpc.spring;

import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

public class FeigeNamespaceHandler extends NamespaceHandlerSupport {

	@Override
	public void init() {
		registerBeanDefinitionParser("service", new ServiceConfigParser());
		registerBeanDefinitionParser("reference", new ReferenceConfigParser());
	}
}
