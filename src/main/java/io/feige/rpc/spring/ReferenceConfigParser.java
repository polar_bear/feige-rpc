package io.feige.rpc.spring;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

public class ReferenceConfigParser implements BeanDefinitionParser {

	@Override
	public BeanDefinition parse(Element e, ParserContext parserContext) {
		RootBeanDefinition beanDefinition = new RootBeanDefinition();
		beanDefinition.setBeanClass(ReferenceBean.class);
		beanDefinition.setLazyInit(false);
		String id=e.getAttribute("id");
		if (id==null) {
			id=e.getAttribute("interface");
		}
		if (id != null && id.length() > 0) {
            if (parserContext.getRegistry().containsBeanDefinition(id))  {
        		throw new IllegalStateException("Duplicate spring bean id " + id);
        	}
            beanDefinition.getPropertyValues().addPropertyValue("interfaceName", e.getAttribute("interface"));
            parserContext.getRegistry().registerBeanDefinition(id, beanDefinition);
        }
		return beanDefinition;
	}

}
