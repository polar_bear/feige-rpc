package io.feige.rpc.spring;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.beans.factory.xml.BeanDefinitionParser;
import org.springframework.beans.factory.xml.ParserContext;
import org.w3c.dom.Element;

public class ServiceConfigParser implements BeanDefinitionParser {

	public ServiceConfigParser() {
	}

	@Override
	public BeanDefinition parse(Element e, ParserContext parserContext) {
		String interfaceName = e.getAttribute("interface");
		String ref = e.getAttribute("ref"); 
		RootBeanDefinition beanDefinition = new RootBeanDefinition();
		beanDefinition.setBeanClass(ServiceBean.class);
		beanDefinition.setLazyInit(false);
        beanDefinition.getPropertyValues().addPropertyValue("interfaceName", interfaceName);
        beanDefinition.getPropertyValues().addPropertyValue("ref", ref);
        parserContext.getRegistry().registerBeanDefinition(interfaceName, beanDefinition);
		return beanDefinition;
	}
}
