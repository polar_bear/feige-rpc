package io.feige.rpc.protocol;

import io.feige.rpc.consumer.RpcConsumerService;
import io.feige.rpc.producer.RpcProducerService;


public interface Protocol {

	public static String Protocol="netty";
	
	Object getWriteObject(String service, String method, Class<?>[] types, Object[] args, long seq);
	
	Object getDecoder();
	
	Object getEncoder();
	
	Object getInvokerResponseHandler(RpcConsumerService rpcConsumerService);
	
	Object getStateCheckChannelHandler(RpcConsumerService rpcConsumerService);
	
	Object getServiceInvokerHandler(RpcProducerService rpcProducerService);
	
}
