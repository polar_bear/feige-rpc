package io.feige.rpc.protocol.nettyobj.pojo;

import java.io.Serializable;

public class IOResponseMessage implements Serializable {

 
	private static final long serialVersionUID = -7768571354570619196L;

	private Object result;

	private Throwable exception;
	
	private long seq;

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public Throwable getException() {
		return exception;
	}

	public void setException(Throwable exception) {
		this.exception = exception;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}
	 
	@Override
	public String toString() {
		return "IOResponseMessage [result=" + result + ", exception="
				+ exception + ", code=" + seq + "]";
	}

}
