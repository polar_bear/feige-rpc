package io.feige.rpc.protocol.kryo;

import io.feige.rpc.consumer.RpcConsumerService;
import io.feige.rpc.consumer.network.handler.InvokerResponseHandler;
import io.feige.rpc.consumer.network.handler.StateCheckChannelHandler;
import io.feige.rpc.producer.RpcProducerService;
import io.feige.rpc.producer.network.handler.ServerInvokerHandler;
import io.feige.rpc.protocol.Protocol;
import io.feige.rpc.protocol.nettyobj.pojo.IORequestMessage;

public class KryoObjectProtocol implements Protocol {

	@Override
	public Object getWriteObject(String service, String method, Class<?>[] types, Object[] args, long seq) {
		IORequestMessage requestMessage=new IORequestMessage();
		requestMessage.setArgs(args);
		requestMessage.setMethod(method);
		requestMessage.setSeq(seq);
		requestMessage.setService(service);
		requestMessage.setTypes(types);
		return requestMessage;
	}

	@Override
	public Object getDecoder() { 
		return new ObjectKryoDecoder();
	}

	@Override
	public Object getEncoder() {
		return new ObjectKryoEncoder();
	}

	@Override
	public Object getInvokerResponseHandler(RpcConsumerService rpcConsumerService) {
		return new InvokerResponseHandler(rpcConsumerService); 
	}

	@Override
	public Object getStateCheckChannelHandler(RpcConsumerService rpcConsumerService) {
		return new StateCheckChannelHandler();
	}

	@Override
	public Object getServiceInvokerHandler(RpcProducerService rpcProducerService) {
		return new ServerInvokerHandler(rpcProducerService);
	}

}
