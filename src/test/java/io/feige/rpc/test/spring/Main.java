package io.feige.rpc.test.spring;

import io.feige.rpc.test.spring.service.SpringTestService;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

	private static ClassPathXmlApplicationContext context;

	public static void main(String[] args) throws Exception {
		String configPath = "classpath*:*.xml";
	    context = new ClassPathXmlApplicationContext(configPath.split("[,\\s]+"));
	    context.start();  
	    Thread.sleep(2000); 
	    SpringTestService se=(SpringTestService) context.getBean("springTestService");
	    while(true){
	    	Thread.sleep(2000);
	    	se.test();
	    }
	  //  ChatMessageService service=(ChatMessageService)context.getBean("testMessage");
	}
}
