package io.feige.rpc.test.spring;

import io.feige.rpc.producer.RpcProducerService;
import io.feige.rpc.test.spring.service.SpringTestService;
import io.feige.rpc.test.spring.service.impl.SpringTestServiceImpl;

public class Producer {

	public static void main(String[] args) {
		RpcProducerService ps=new RpcProducerService();
		ps.start();
		ps.exportService(SpringTestService.class, new SpringTestServiceImpl());
		
	}

}
